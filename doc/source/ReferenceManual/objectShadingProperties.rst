.. include:: ../macros.hrst
.. include:: ../abbreviations.hrst

.. _chapter:ObjectShadingProperties:

Object Shading Properties
#########################

When visualizing any data it is important to control how the data is actually rendered.
Most of the properties controlling the appearance of an object can be found in the ``Display Properties`` section in the object inspector (see section :ref:`display properties<sec:RenderView:DisplayProperties>`).
This chapter tries to entirely cover the lighting properties of an object when using the ``Surface`` representation.

Flat and Gouraud Lighting
=========================

These lighting models are intended to be easy-to-use basic models for general visualisation purposes.
Both of these models are mainly controlled by the same parameters.
The only difference between the two is that the flat shading model does not interpolate the normals of the rendered surfaces,
meaning that a Gouraud lighting with no normal array will produce the same result as the flat shading model.

The parameters for these two models are :

* Specular: how specular the object is, that is how much of the light it will reflect.
* Specular Color: the color of the resulting specular.
* Specular Power: how much the specular is spread on the object. The lower the wider it spreads.
* Luminosity: how much light the object emits. Only takes effect with a pathtracer backend.
* Ambient: coefficient for the ambient lighting.
* Diffuse: coefficient for the diffuse lighting.
* Texture Coordinates: the texture coordinates to use when applying a texture. This is needed to be able to use the `Texture` property.
* Texture: also called albedo, this is the perceived color of the object.
* Show Texture On Backface: whether or not to show the texture on the backface of the surface.
* Flip Texture: if on, flip the color texture.

Some other properties specific to the Gouraud shading :

* Normal Array: what array to use as the normal array
* Tangent Array: what array to use as the tangent array. Only needed when using the `Normal Texture`.
* Normal Texture: a normal map. This allows to have more precise shading without the need to subdivide the geometry. Stores the normal direction `{x,y,z}` in the RGB channels.
* Normal Scale: scale factor for the normal map.

PBR Lighting
============

This lighting model is more complex than the previous ones but allows a wider range of effect on the objects
and a more realistic rendering. Since this model is more complex to grasp, several blog posts have been written to
explain how to use it (links to blogs are provided in the relevant sections below).

Basic parameters for this model are :

* Metallic: whether the object is metallic (= 1.0) or non-metallic / dielectric (= 0.0). For most materials, the value is either 0.0 or 1.0 but any value in between is valid.
* Roughness: parameter used to specify how glossy an object is.
* Luminosity: how much light the object emits. Only takes effect with a pathtracer backend.
* Diffuse: the global amount of light reflected by the object. Should usually be set to 1.0 as it is not an actual parameter of the theorical PBR shading model but it can be used for artistic effects.

.. figure:: ../images/PBR_Parameters_Basic_Properties.jpg
   :width: 1000px

Following are more advanced properties. They can be hard to understand visually without knowing the theory behind it so
it is advised to refer to the linked blog posts when using them for the first time.

* Anisotropy: the strength of the anisotropy (between 0.0 and 1.0). 0.0 means an isotropic material, 1.0 means a fully anisotropic material.
* Anisotropy Rotation: rotates the direction of the anisotropy (ie. the tangent) around the normal counter-clockwise (between 0.0 and 1.0). A value of 1.0 corresponds to a rotation of `2 * PI`.

  * anisotropy parameters are fully detailed here: `anisotropy blog <https://www.kitware.com/pbr-journey-part-2-anisotropy-model-with-vtk/>`_

.. figure:: ../images/PBR_anisotropy_parameters.jpg
   :width: 1000px

* Coat Strength: the strength of the coat layer, between 0.0 and 1.0. 0 means no clear coat. This parameter can be considered as the thickness of the coating.
* Coat Roughness: the roughness of the coat layer.
* Coat Color: the color of the coat layer. Specular reflections on the coat layer are always white, but this parameter modifies the radiance that passes through it.
* Base IOR: the refractive index of the base layer.
* Coat IOR: the refractive index of the coat layer.
* Edge Tint: the color of the reflection on the edges at grazing angles for a metallic material.

  * coat parameters are fully detailed here: `clear coat blog <https://www.kitware.com/pbr-journey-part-3-clear-coat-model-with-vtk/>`_

.. figure:: ../images/PBR_coat_parameters.jpg
   :width: 1000px

Finally, some parameters can also be mapped using textures.
As in the Gouraud shading model, a proper value for the `Texture Coordinates` and `Normal Array` properties will be needed in order to apply textures.

* Base Color Texture: also called albedo, this is the perceived color of the object, the diffuse color for non-metallic objects or the specular color for metallic objects.
* Normal Texture: a normal map. This allows to have more precise shading without the need to subdivide the geometry. Stores the normal direction `{x,y,z}` in the RGB channels.
* Material Texture: ambient Occlusion/Roughness/Metallic factors on the Red/Green/Blue channels. Also called ORM texture.
* Coat Normal Texture: a normal map for the clear coat layer.
* Anisotropy Texture: texture that controls the anisotropy strength on the red channel, while the green channel holds the anisotropy rotation. The blue channel is discarded.
* Emissive Texture: for light-emitting texture. Note that the material will not actually emit light, and the texture is ignored by OSPRay and NVidia pathtracing backends.

.. \begin{TODO}
     add a volume representation section.
   \end{TODO}
